# README #

GoLang storage of JSON structs into a flat file

### What is this repository for? ###

* Library for storage and retrieval of JSON structs in a flat file
* Version 1.0

### How do I get set up? ###

* Simply import to your project and use

### Contribution guidelines ###

* Writing tests
* Pull request if tests pass

### Who do I talk to? ###

* Tom Ogoma