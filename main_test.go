package jsonfile

import (
	"strings"
	"testing"
)

func TestWriteToJsonFile_singleValue(t *testing.T) {

	myData := DataStruct{"key", "value"}

	if err := myData.WriteToJsonFile("mytestfile.json"); err != nil {
		t.Error(err)
	}

}

func TestWriteToJsonFile_multipleValues(t *testing.T) {

	myData1 := DataStruct{"key", "value"}
	myData2 := DataStruct{"key2", "value2"}

	if err := myData1.WriteToJsonFile("mytestfile.json"); err != nil {
		t.Error(err)
	}

	if err := myData2.WriteToJsonFile("mytestfile.json"); err != nil {
		t.Error(err)
	}

}

func TestReadFromJsonFile(t *testing.T) {

	var dataset []DataStruct
	var err error

	dataset, err = ReadFromJsonFile("mytestfile.json")

	t.Log(dataset)

	if err != nil {
		t.Error(err)
	}

	for _, data := range dataset {
		if !strings.HasPrefix(data.Key, "key") {
			t.Error("Keys do not match")
		}

		if !strings.HasPrefix(data.Data, "value") {
			t.Error("Values do not match")
		}
	}

}
