package jsonfile

import (
	"bufio"
	"encoding/json"
	"io"
	"os"
)

type DataStruct struct {
	Key  string
	Data string
}

func (d *DataStruct) WriteToJsonFile(filename string) (err error) {

	var data []byte
	var f *os.File

	data, err = json.Marshal(d)
	if err != nil {
		return err
	}

	data = append(data, byte('\n'))

	f, err = os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0666)
	if err != nil {
		if err.Error() == "open mytestfile.json: no such file or directory" {
			f, err = os.Create(filename)
		} else {
			return
		}
	}
	defer f.Close()
	f.Write(data)

	return
}

func ReadFromJsonFile(filename string) (dataset []DataStruct, err error) {

	var filepointer *os.File

	filepointer, err = os.Open(filename)
	if err != nil {
		return
	}
	defer filepointer.Close()

	reader := bufio.NewReader(filepointer)
	line, file_err := reader.ReadBytes('\n')
	for file_err == nil {
		var data DataStruct
		err = json.Unmarshal(line, &data)
		if err != nil {
			line, file_err = reader.ReadBytes('\n')
			continue
		}
		dataset = append(dataset, data)
		line, file_err = reader.ReadBytes('\n')
	}
	if file_err != io.EOF {
		//  I/O error otheer than end of file error
		err = file_err
	}
	return
}
